package health

import "reflect"

type Checker interface {
	HealthName() string
	HealthCode() string
	HealthCheck() (bool, error)
}

func GetHeathChecker(obj interface{}) []Checker {
	return fillHealthChecker(obj, nil)
}

func getWithConfigHealthChecker(obj interface{}) Checker {
	switch v := obj.(type) {
	case Checker:
		return v
	default:
		return nil
	}
}

func fillHealthChecker(obj interface{}, list []Checker) []Checker {
	if el := getWithConfigHealthChecker(obj); el != nil {
		list = append(list, el)
	}

	r := reflect.ValueOf(obj)
	if r.Type().Kind() == reflect.Ptr {
		r = reflect.Indirect(r)
	}
	if r.Type().Kind() != reflect.Struct {
		return list
	}

	count := r.NumField()
	for i := 0; i < count; i++ {
		if r.Field(i).Type().Kind() == reflect.Ptr && r.Field(i).IsNil() {
			continue
		}
		list = fillHealthChecker(r.Field(i).Interface(), list)
	}

	return list
}
